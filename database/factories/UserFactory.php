<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models;
use Faker\Generator as Faker;
$fakerVN = \Faker\Factory::create('vi_VN');
$factory->define( App\Models\User::class, function (Faker $faker) use ($fakerVN) {
    return [
        'mail_address' => $fakerVN->unique()->safeEmail,
        'name' => $fakerVN->name,
        'password' => Hash::make(12345678),  
        'address' => $fakerVN->address,
        'phone' => $fakerVN->phoneNumber,
    ];
});
