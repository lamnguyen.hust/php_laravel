
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/style-listuser.css') }}" />
    <title>List User Of Database</title>
  </head>
  <body>
    <div class="table-responsive">
      <h2>Table Data User</h2>
      @if (session('status'))
        <div class="alert alert-info">{{session('status')}}</div>
       
    @endif
        <table class="table" id="table1">
          <thead>
            <tr>
                <th>User ID</th>
                <th>Email Address</th>
                <th>Name</th>
                <th>Address</th>
                <th>Phone</th>
                <th>Create at</th>
                <th>Update at</th>
            </tr>
          </thead>
          <tbody>
            @foreach($users as $user)
            <tr>
                <th scope="col">{{ $user-> id}}</th>
                <th scope="col">{{ $user-> mail_address}}</th>
                <th scope="col">{{ $user-> name}}</th>
                <th scope="col">{{ $user-> address}}</th>
                <th scope="col">{{ $user-> phone}}</th>
                <th scope="col">{{ $user-> created_at}}</th>
                <th scope="col">{{ $user-> updated_at}}</th>
               
            </tr>
            
            @endforeach
          </tbody>
        </table>
        <div id="footer">
           <a href="{{ url('register') }}" >Link To Register</a> 
           <span><?php echo $users->render(); ?></span> 
        </div>
        
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>