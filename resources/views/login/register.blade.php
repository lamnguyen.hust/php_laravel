<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/style-register.css') }}" />
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

    <title>Register</title>
  </head>
  <body>
    <div class="container register-form">
            <div class="form">
                <div class="note">
                    <h2>Form Register</h2>
                </div>

                <form class="form-content" method="post" action="/register/sent">
                  @csrf
                  <div class="row">
                      <div class="col-md-6">
                          <div class="form-group">
                             <input name="name" type="text" class="form-control @error('name') is-invalid @enderror" placeholder="Your Name *" value=""/>
                             <p class="help is-danger">{{ $errors->first('name') }}</p>
                          </div>
        
                          <div class="form-group">
                              <input name="phone" type="text" class="form-control @error('phone') is-invalid @enderror" placeholder="Phone Number *" value=""/>
                              <p class="help is-danger">{{ $errors->first('phone') }}</p>
                          </div>
                          <div class="form-group">
                              <input name="mail_address" type="email" class="form-control @error('mail_address') is-invalid @enderror" placeholder="Email *" value=""/>
                              <p class="help is-danger">{{ $errors->first('mail_address') }}</p>
                          </div>

                          {{-- @if ($errors->any())
                              <div class="alert alert-danger">
                                  <ul>
                                      @foreach ($errors->all() as $error)
                                          <li>{{ $error }}</li>
                                      @endforeach
                                  </ul>
                              </div>
                          @endif --}}

                      </div>
                      <div class="col-md-6">
                           <div class="form-group">
                              <input name="address" type="text" class="form-control @error('address') is-invalid @enderror" placeholder="Address *" value=""/>
                              <p class="help is-danger">{{ $errors->first('address') }}</p>

                          </div>
                          <div class="form-group">
                              <input name="password" type="password" class="form-control @error('password') is-invalid @enderror" placeholder="Password *" value=""/>
                              <p class="help is-danger">{{ $errors->first('password') }}</p>

                          </div>
                          <div class="form-group">
                              <input name="password_confirmation" type="password" class="form-control @error('password_confirmation') is-invalid @enderror" placeholder="Password Confirm *" value=""/>
                              <p class="help is-danger">{{ $errors->first('password_confirmation') }}</p>

                          </div>
                         
                          <button  type="submit" class="btnSubmit" name="submit_register">Submit</button>
                      </div>
                  </div>
             
              </form>
            

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
  </body>
</html>