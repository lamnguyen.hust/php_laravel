<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LandingPageController extends Controller
{
    function showLandingPage () {
        return view('welcome');
    }
}
