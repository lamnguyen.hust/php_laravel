<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\RegisterRequest;
use Illuminate\Support\Facades\DB;

class RegisterController extends Controller
{
    function registerAccount(RegisterRequest $request) {

        DB::table('users')->insert([
            'mail_address' => $request->mail_address,
            'password' => $request->password,
            'name' => $request->name,
            'address' => $request->address,
            'phone' => $request->phone,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);
        $request->session()->flash('status', 'Tạo tài khoản thành công!');
        return redirect()->route('showListUser');

    }
    function showDatabase() {
        $users = DB::table('users')->orderBy('mail_address')->paginate(20);
      //  select('SELECT * FROM users ORDER BY mail_address', [20]);

        return view('login.listUser', ['users' => $users]);
    }
    function showRegister() {
        return view('login.register');
    }
}
