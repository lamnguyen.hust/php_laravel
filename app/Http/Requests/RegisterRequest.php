<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Unique;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'mail_address' => ['max:100',
                               'required',
                               'unique:users',],
            'password' => ['required',
                            'min:3',],
            'password_confirmation' => 'same:password',
            'phone'=> ['numeric'],
        ];
    }
    public function attributes()
    {
        return [
            'mail_address' => 'địa chỉ email',
            'password' => 'mật khẩu',
            'password_confirmation' => 'nhập lại mật khẩu',
            'phone' => 'số điện thoại'
        ];
    }
     /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'mail_address.unique' => 'Địa chỉ email đã tồn tại',
            'phone.numeric' => 'Số điện thoại phải là số',
            'password.min' => 'Mật khẩu quá ngắn tối thiểu 3 kí tự',
            'password_confirmation.same' => 'Nhập lại mật khẩu không đúng'
        ];
    }

}
